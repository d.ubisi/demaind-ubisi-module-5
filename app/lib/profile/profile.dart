import 'dart:convert';
import 'dart:ffi';

import 'package:app/model.dart';
import 'package:firebase_database/firebase_database.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class Profile extends StatefulWidget {
  const Profile({Key? key}) : super(key: key);

  @override
  State<Profile> createState() => _ProfileState();
}

class _ProfileState extends State<Profile> {
  final ref = FirebaseDatabase.instance.ref();

  String name = "";
  String username = "";

  late Future<Map<String, dynamic>> myFuture;

  Future<Map<String, dynamic>> getData() async {
    final snapshot = await ref.child('users/$username').get();
    name = snapshot.value.toString();
    return json.decode(json.encode(snapshot.value));
  }

  @override
  void initState() {
    myFuture = getData();
    super.initState();
  }

  Future<void> delete(String s) async {
    await ref.child(s).remove();
  }

  late TextEditingController username1;
  late TextEditingController name1;
  late TextEditingController lastName;
  late TextEditingController email;
  late TextEditingController password;

  @override
  Widget build(BuildContext context) {
    username = context.watch<Auth>().getUsername();
    return FutureBuilder(
        future: myFuture,
        initialData: {},
        builder: (BuildContext context, AsyncSnapshot<Map> snapshot) {
          if (snapshot.connectionState == ConnectionState.waiting) {
            return const Center(child: CircularProgressIndicator());
          }
          Map? content = snapshot.data![username];

          username1 = TextEditingController(text: content!["username"]);
          name1 = TextEditingController(text: content["name"]);
          lastName = TextEditingController(text: content["surname"]);
          email = TextEditingController(text: content["email"]);
          password = TextEditingController(text: content["password"]);

          return Scaffold(
            appBar: AppBar(
              backgroundColor: Colors.green,
              title: const Text("Profile"),
              actions: [
                IconButton(
                    onPressed: () async {
                      delete("users/$username");
                      Navigator.pushNamed(context, "/");
                    },
                    icon: const Icon(Icons.delete))
              ],
            ),
            body: ListView(
              children: [
                const SizedBox(
                  height: 25,
                ),
                Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    SizedBox(
                      width: 255,
                      child: TextFormField(
                        controller: username1,
                        decoration: const InputDecoration(
                            label: Text("username"), enabled: false),
                      ),
                    ),
                    SizedBox(
                      width: 255,
                      child: TextFormField(
                        controller: name1,
                        decoration: const InputDecoration(
                          label: Text("name"),
                          enabled: false,
                        ),
                        enabled: false,
                      ),
                    ),
                    SizedBox(
                      width: 255,
                      child: TextFormField(
                        controller: lastName,
                        decoration: const InputDecoration(
                          label: Text("surname"),
                        ),
                        enabled: false,
                      ),
                    ),
                    SizedBox(
                      width: 255,
                      child: TextFormField(
                        controller: email,
                        decoration: const InputDecoration(label: Text("email")),
                      ),
                    ),
                    SizedBox(
                      width: 255,
                      child: TextFormField(
                        controller: password,
                        obscureText: true,
                        decoration:
                            const InputDecoration(label: Text("password")),
                      ),
                    ),
                    const SizedBox(
                      height: 15,
                    ),
                    ElevatedButton(
                      style: ElevatedButton.styleFrom(primary: Colors.green),
                      onPressed: (() {
                        final newPostKey = FirebaseDatabase.instance
                            .ref()
                            .child('users/$username');

                        final postData = {
                          'username': username1.text,
                          'name': name1.text,
                          'surname': lastName.text,
                          'email': email.text,
                          'password': password.text,
                        };

                        final Map<String, Map> updates = {};

                        updates["users/$username"] = postData;
                        FirebaseDatabase.instance
                            .ref()
                            .update(updates)
                            .then((_) {
                          // Data saved successfully!
                          ScaffoldMessenger.of(context)
                              .showSnackBar(const SnackBar(
                            content: Text('Updated'),
                          ));
                        }).catchError((error) {
                          // The write failed...
                          ScaffoldMessenger.of(context)
                              .showSnackBar(const SnackBar(
                            content: Text('Failed to update'),
                          ));
                        });
                      }),
                      child: const Text("confirm"),
                    ),
                  ],
                ),
              ],
            ),
          );
        });
  }
}
