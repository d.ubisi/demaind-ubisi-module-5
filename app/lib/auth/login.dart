import 'package:app/model.dart';
import 'package:firebase_database/firebase_database.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class Login extends StatefulWidget {
  const Login({Key? key}) : super(key: key);

  @override
  State<Login> createState() => _LoginState();
}

class _LoginState extends State<Login> {
  late String user;

  TextEditingController username = TextEditingController();
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          SizedBox(
            width: 255,
            child: TextField(
              controller: username,
              decoration: const InputDecoration(label: Text("username")),
            ),
          ),
          const SizedBox(
            width: 255,
            child: TextField(
              decoration: InputDecoration(label: Text("password")),
            ),
          ),
          const SizedBox(
            height: 15,
          ),
          ElevatedButton(
            style: ElevatedButton.styleFrom(primary: Colors.green),
            onPressed: () async {
              final Auth auth = Provider.of<Auth>(context, listen: false);

              if (await getAuth(username: username.text)) {
                user = username.text;
                auth.setEmail(user);
                Navigator.pushNamed(context, "/home");
              } else {
                ScaffoldMessenger.of(context).showSnackBar(SnackBar(
                  content: const Text('User not found'),
                  action: SnackBarAction(
                    label: 'Register',
                    onPressed: () {
                      // Some code to undo the change.
                      Navigator.pushNamed(context, "/register");
                    },
                  ),
                ));
              }
            },
            child: const Text("Login"),
          ),
          const Text("Fogot password?"),
          Padding(
            padding: const EdgeInsets.all(8.0),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                const Text("Don't have an account?"),
                InkWell(
                  child: const Text(
                    " register",
                    style: TextStyle(color: Colors.green),
                  ),
                  onTap: () => {Navigator.pushNamed(context, "/register")},
                )
              ],
            ),
          )
        ],
      ),
    );
  }

  Future<bool> getAuth({required String username}) async {
    final ref = FirebaseDatabase.instance.ref();
    final snapshot = await ref.child('users/$username').get();
    if (snapshot.exists) {
      return true;
    }
    return false;
  }
}
