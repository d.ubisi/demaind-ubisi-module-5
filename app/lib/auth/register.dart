import 'package:flutter/material.dart';
import 'package:firebase_database/firebase_database.dart';
import 'package:flutter/scheduler.dart';

class Register extends StatefulWidget {
  const Register({Key? key}) : super(key: key);

  @override
  State<Register> createState() => _RegisterState();
}

class _RegisterState extends State<Register> {
  TextEditingController username = TextEditingController();
  TextEditingController name = TextEditingController();
  TextEditingController lastName = TextEditingController();
  TextEditingController email = TextEditingController();
  TextEditingController password = TextEditingController();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: ListView(
        children: [
          const Padding(
            padding: EdgeInsets.all(8.0),
            child: Text("Regiser"),
          ),
          const SizedBox(
            height: 25,
          ),
          Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              SizedBox(
                width: 255,
                child: TextField(
                  controller: username,
                  decoration: const InputDecoration(label: Text("username")),
                ),
              ),
              SizedBox(
                width: 255,
                child: TextField(
                  controller: name,
                  decoration: const InputDecoration(label: Text("name")),
                ),
              ),
              SizedBox(
                width: 255,
                child: TextField(
                  controller: lastName,
                  decoration: const InputDecoration(label: Text("surname")),
                ),
              ),
              SizedBox(
                width: 255,
                child: TextField(
                  controller: email,
                  decoration: const InputDecoration(label: Text("email")),
                ),
              ),
              SizedBox(
                width: 255,
                child: TextField(
                  controller: password,
                  decoration: const InputDecoration(label: Text("password")),
                ),
              ),
              const SizedBox(
                height: 15,
              ),
              ElevatedButton(
                style: ElevatedButton.styleFrom(primary: Colors.green),
                onPressed: (() async {
                  DatabaseReference ref =
                      FirebaseDatabase.instance.ref("users/${username.text}");

                  await ref.set({
                    "username": username.text,
                    "name": name.text,
                    "surname": lastName.text,
                    "email": email.text,
                    "password": password.text
                  }).then((_) {
                    // Data saved successfully!
                    ScaffoldMessenger.of(context).showSnackBar(const SnackBar(
                      content: Text('Registered'),
                    ));
                  }).catchError((error) {
                    // The write failed...
                    ScaffoldMessenger.of(context).showSnackBar(const SnackBar(
                      content: Text('Failed try again later'),
                    ));
                  });
                  // ignore: use_build_context_synchronously
                  Navigator.pushNamed(context, "/");
                }),
                child: const Text("Sign Up"),
              ),
              Padding(
                padding: const EdgeInsets.all(8.0),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    const Text("Already have an account?"),
                    InkWell(
                      child: const Text(
                        " login",
                        style: TextStyle(color: Colors.green),
                      ),
                      onTap: () {
                        Navigator.pushNamed(context, "/");
                      },
                    )
                  ],
                ),
              )
            ],
          ),
        ],
      ),
    );
  }
}
